package com.litmantech.instaauto.di.launchactivity;

import com.litmantech.instaauto.di.launchactivity.modules.AndroidViewModelModule;
import com.litmantech.instaauto.di.launchactivity.modules.ViewByIdModule;
import com.litmantech.instaauto.ui.launch.LaunchActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AndroidViewModelModule.class, ViewByIdModule.class})
public interface LaunchAppComponent {

    void inject(LaunchActivity launchActivity);
}
