package com.litmantech.instaauto.di.launchactivity.modules;

import com.litmantech.instaauto.ui.launch.viewmodel.TextToggleViewModel;

import javax.inject.Singleton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import dagger.Module;
import dagger.Provides;

@Module
public class AndroidViewModelModule {

    private final AppCompatActivity activity;

    public AndroidViewModelModule(AppCompatActivity activity) {
        this.activity = activity;
    }


    @Provides
    @Singleton
    TextToggleViewModel textToggleViewModel() {
        return ViewModelProviders.of(activity).get(TextToggleViewModel.class);
    }
}
