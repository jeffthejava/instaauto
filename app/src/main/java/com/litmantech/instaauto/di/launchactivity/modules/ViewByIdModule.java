package com.litmantech.instaauto.di.launchactivity.modules;

import android.widget.Button;
import android.widget.TextView;

import com.litmantech.instaauto.R;

import javax.inject.Named;
import javax.inject.Singleton;

import androidx.appcompat.app.AppCompatActivity;
import dagger.Module;
import dagger.Provides;

@Module
public class ViewByIdModule {

    private final AppCompatActivity activity;


    public ViewByIdModule(AppCompatActivity activity) {
        this.activity = activity;
    }


    @Provides
    @Singleton
    @Named("toggleButton")
    Button toggleButton() {
        return activity.findViewById(R.id.toggle_button);
    }

    @Provides
    @Singleton
    @Named("messageView")
    TextView messageView() {
        return activity.findViewById(R.id.launch_message_view);
    }
}
