package com.litmantech.instaauto.ui.launch.viewmodel;

import android.app.Application;
import android.content.res.Resources;
import android.text.TextUtils;

import com.litmantech.instaauto.R;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class TextToggleViewModel extends AndroidViewModel {
    final String helloMessage;
    final String appNameMessage;

    MutableLiveData<String> displayMessage = new MutableLiveData<>();

    public TextToggleViewModel(@NonNull Application application) {
        super(application);
        Resources resources = application.getResources();
        appNameMessage = resources.getString(R.string.app_name);
        helloMessage = resources.getString(R.string.hello_message);

        displayMessage.postValue(helloMessage);
    }

    public void toggleMessage() {
        String currentMessage = displayMessage.getValue();

        if (TextUtils.equals(currentMessage, helloMessage)) {
            displayMessage.postValue(appNameMessage);
        } else if (TextUtils.equals(currentMessage, appNameMessage)) {
            displayMessage.postValue(helloMessage);
        }
    }

    public LiveData<String> getDisplayMessage() {
        return displayMessage;
    }
}
