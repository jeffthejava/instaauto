package com.litmantech.instaauto.ui.launch;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.litmantech.instaauto.R;
import com.litmantech.instaauto.di.launchactivity.DaggerLaunchAppComponent;
import com.litmantech.instaauto.di.launchactivity.modules.AndroidViewModelModule;
import com.litmantech.instaauto.di.launchactivity.modules.ViewByIdModule;
import com.litmantech.instaauto.ui.launch.viewmodel.TextToggleViewModel;

import javax.inject.Inject;
import javax.inject.Named;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

public class LaunchActivity extends AppCompatActivity {

    @Inject
    TextToggleViewModel textToggleViewModel;

    @Inject
    @Named("toggleButton")
    Button toggleButton;

    @Inject
    @Named("messageView")
    TextView messageView;

    Observer textToggleObserver = message -> messageView.setText((String) message);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        DaggerLaunchAppComponent.builder()
                .androidViewModelModule(new AndroidViewModelModule(this))
                .viewByIdModule(new ViewByIdModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        textToggleViewModel.getDisplayMessage().observe(this, textToggleObserver);
        toggleButton.setOnClickListener(v -> textToggleViewModel.toggleMessage());
    }

    @Override
    protected void onPause() {
        super.onPause();
        textToggleViewModel.getDisplayMessage().removeObserver(textToggleObserver);
    }
}
