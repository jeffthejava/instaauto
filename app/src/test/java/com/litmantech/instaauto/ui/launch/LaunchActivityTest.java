package com.litmantech.instaauto.ui.launch;


import android.widget.Button;
import android.widget.TextView;

import com.litmantech.instaauto.R;
import com.litmantech.instaauto.ui.launch.viewmodel.TextToggleViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class LaunchActivityTest {


    ActivityController<LaunchActivity> activityController;

    LaunchActivity launchActivity;

    @Before
    public void setUp() throws Exception {
        activityController = Robolectric.buildActivity(LaunchActivity.class);
        launchActivity = activityController.get();
    }

    @Test
    public void during_onCreate_assign_button_view_and_text_view() {
        assertNull(launchActivity.messageView);
        assertNull(launchActivity.toggleButton);


        activityController.create();


        assertEquals(launchActivity.messageView, launchActivity.findViewById(R.id.launch_message_view));
        assertEquals(launchActivity.toggleButton, launchActivity.findViewById(R.id.toggle_button));
    }

    @Test
    public void during_onCreate_init_TextToggleViewModel() {
        assertNull(launchActivity.textToggleViewModel);


        activityController.create();


        assertNotNull(launchActivity.textToggleViewModel);
    }

    @Test
    public void during_onResume_observe_text_changes_in_textToggle_view_model() {
        activityController.setup();
        TextToggleViewModel mockTextToggleViewModel = mock(TextToggleViewModel.class);
        LiveData mockDisplayMessage = mock(LiveData.class);
        when(mockTextToggleViewModel.getDisplayMessage()).thenReturn(mockDisplayMessage);
        launchActivity.textToggleViewModel = mockTextToggleViewModel;


        activityController.resume();
        Observer observer = launchActivity.textToggleObserver;


        verify(mockDisplayMessage).observe(launchActivity, observer);
    }


    @Test
    public void when_observer_is_triggered_then_update_text_on_screen() {
        activityController.setup();
        TextView mockTextView = mock(TextView.class);
        launchActivity.messageView = mockTextView;
        Observer textToggleObserver = launchActivity.textToggleObserver;
        String message = "--- New Message";

        textToggleObserver.onChanged(message);

        verify(mockTextView).setText(message);
    }

    @Test
    public void when_button_pressed_then_toggle_message_in_text_view_model() {
        activityController.setup();
        TextToggleViewModel mockTextToggleViewModel = mock(TextToggleViewModel.class);
        launchActivity.textToggleViewModel = mockTextToggleViewModel;

        Button toggleButton = launchActivity.toggleButton;
        toggleButton.performClick();

        verify(mockTextToggleViewModel).toggleMessage();
    }

    @Test
    public void during_on_pause_remove_Observer() {
        activityController.setup();
        TextToggleViewModel mockTextToggleViewModel = mock(TextToggleViewModel.class);
        LiveData mockDisplayMessage = mock(LiveData.class);
        when(mockTextToggleViewModel.getDisplayMessage()).thenReturn(mockDisplayMessage);
        launchActivity.textToggleViewModel = mockTextToggleViewModel;


        activityController.pause();


        verify(mockDisplayMessage).removeObserver(launchActivity.textToggleObserver);
    }
}
