package com.litmantech.instaauto.ui.launch.viewmodel;

import android.app.Application;
import android.content.res.Resources;

import com.litmantech.instaauto.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class TextToggleViewModelTest {


    MutableLiveData<String> displayMessage;

    String sampleHelloMessage = "---- Hello Message";
    String sampleAppNameMessage = "---- App Name";

    TextToggleViewModel textToggleViewModel;


    @Before
    public void setUp() throws Exception {
        Application mockApplication = setupMockApplication(sampleHelloMessage, sampleAppNameMessage);

        textToggleViewModel = new TextToggleViewModel(mockApplication);
        displayMessage = textToggleViewModel.displayMessage;
    }


    @Test
    public void during_init_of_constructor_set_addName_and_helloMessage() {
        String sampleHello = "hello";
        String sampleAppName = "App Name";
        Application mockApplication = setupMockApplication(sampleHello, sampleAppName);

        TextToggleViewModel toggleViewModel = new TextToggleViewModel(mockApplication);

        assertEquals(sampleHello, toggleViewModel.helloMessage);
        assertEquals(sampleAppName, toggleViewModel.appNameMessage);
    }

    @Test
    public void during_init_of_constructor_set_displayMessage_as_hello() {
        String sampleHello = "hello";
        String sampleAppName = "App Name";
        Application mockApplication = setupMockApplication(sampleHello, sampleAppName);

        TextToggleViewModel toggleViewModel = new TextToggleViewModel(mockApplication);
        LiveData displayMessage = toggleViewModel.getDisplayMessage();


        assertEquals(sampleHello, displayMessage.getValue());
    }

    @Test
    public void if_current_message_is_helloMessage_then_set_to_appNameMessage() {
        displayMessage.postValue(sampleHelloMessage);

        textToggleViewModel.toggleMessage();

        assertEquals(sampleAppNameMessage, displayMessage.getValue());
    }

    @Test
    public void if_current_message_is_appNameMessage_then_set_to_helloMessage() {
        displayMessage.postValue(sampleAppNameMessage);

        textToggleViewModel.toggleMessage();

        assertEquals(sampleHelloMessage, displayMessage.getValue());
    }


    Application setupMockApplication(String helloMessage, String appNameMessage) {
        Application application = mock(Application.class);
        Resources resources = mock(Resources.class);

        when(application.getResources()).thenReturn(resources);
        when(resources.getString(R.string.hello_message)).thenReturn(helloMessage);
        when(resources.getString(R.string.app_name)).thenReturn(appNameMessage);

        return application;
    }
}